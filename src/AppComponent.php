<?php

namespace YiiNavigationComponent;

class AppComponent extends \CApplicationComponent {

	public $navigationModel = 'Navigation';
	public $navigationItemModel = 'NavigationItem';

	public $types = [];

/*	public function init() {
		parent::init();

		$className = get_class($this);

		if (!\Yii::getPathOfAlias($className))
			\Yii::setPathOfAlias($className, realpath(dirname(__FILE__)));
	}*/

	public function getItemTypesOptions() {

		static $options = null;

		if (null === $options) {

			$options = [];
			foreach ($this->types as $name => $config) {

				$model = $this->getItemTypeModel($config);
				$options[$name] = $model->getTypeTitle();
			}
		}

		return $options;
	}

	public function getItemTypeModel($config, $params = []) {

		if (is_string($config))
			$config = ['class' => $config];

		return \Yii::createComponent(\CMap::mergeArray($config, $params));
	}

	public function getItemModelByType($type, $params = []) {
		return isset($this->types[$type]) ? $this->getItemTypeModel($this->types[$type], $params) : null;
	}

}