<?php

namespace YiiNavigationComponent\Widget;

class Menu extends \CWidget
{

	public $items;
	public $id;
	public $htmlOptions = [];
	public $activeItemsId = [];
	public $activeItemsUrl = [];

	public function init()
	{

		if (empty($this->id))
			$this->id = $this->getId();

		$this->activeItemsUrl[] = \Yii::app()->getRequest()->getUrl();

		parent::init();
	}

	/**
	 *
	 * @param array $aData
	 * @param int $iPId
	 * @param int $iDepth
	 * @param int $level
	 * @return array
	 */
	protected function _getTreeRecursive(&$aData, $iPId = 0, $iDepth = 20, $level = 0)
	{

		$aTree = [];
		$iDepth -= 1;
		if (isset($aData[$iPId])) {
			foreach ($aData[$iPId] as $itm) {

				$aChld = [
					'label' => $itm->getTitleFinal(),
					'url' => $itm->url,
					'itemOptions' => [
						'class' => 'level' . $level . (empty($itm->css_class) ? '' : ' ' . $itm->css_class),
					],
					'linkOptions' => [
						'title' => empty($itm->anchor_title) ? $itm->getTitleFinal() : $itm->anchor_title,
					],
				];

				if ([] !== $this->activeItemsId && in_array($itm->id, $this->activeItemsId)) {
					$aChld['active'] = true;
				}

				if ([] !== $this->activeItemsUrl && in_array($itm->url, $this->activeItemsUrl)) {
					$aChld['active'] = true;
				}

				if ($iDepth <> 0) {

					$aChld['items'] = $this->_getTreeRecursive($aData, (int)$itm->id, $iDepth, $level + 1);

					$navModel = $itm->getTypeModel();
					if (($iDepth - 1) <> 0 && $navModel->getHasSubitems())
						foreach ($navModel->getSubitems($level + 1) as $subItm)
							$aChld['items'][] = $subItm;

					if (sizeof($aChld['items']) > 0)
						$aChld['itemOptions']['class'] .= ' submenu';
				}

				if ($itm->expanded == \NavigationItem::EXPANDED_YES) {

					if (isset($aChld['items']) && [] !== $aChld['items'])
						foreach ($aChld['items'] as $expItm)
							$aTree[] = $expItm;
				} else
					$aTree[] = $aChld;
			}
		}

		return $aTree;
	}

	function run()
	{

		$this->widget('zii.widgets.CMenu', [
			'id' => $this->id,
			'encodeLabel' => true,
			'linkLabelWrapper' => 'span',
			'firstItemCssClass' => 'first',
			'lastItemCssClass' => 'last',
			'htmlOptions' => $this->htmlOptions,
			'activateParents' => true,
			'items' => $this->_getTreeRecursive($this->items),
		]);
	}

}
