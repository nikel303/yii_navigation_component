<?php

namespace YiiNavigationComponent\Type;

abstract class Type extends \CFormModel {

    abstract public function getTypeTitle();

    abstract public function getData();

    abstract public function attributeWidgets();

    public function getHasSubitems() {
        return false;
    }

    public function getSubitems($level) {
        return [];
    }

    public function getTypeCode() {
        return mb_strtolower(__CLASS__);
    }

}
