<?php

namespace YiiNavigationComponent\Type;

class Url extends Type {

    public $url;

    public function getTypeTitle() {
        return 'Ссылка';
    }

    public function rules() {
        return [
            ['url', 'required'],
            ['url', 'length', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'url' => 'Ссылка',
        ];
    }

    public function attributeWidgets() {
        return [
            'url' => [
                'type' => 'url',
                'htmlOptions' => [
                    'maxlength' => 255,
                    'class' => 'span5',
                ],
                'hint' => 'Укажите относительный или абсолютный URL (http://yandex.ru)',
            ],
        ];
    }

    public function getData() {
        return [
            'url' => $this->url,
            'title' => $this->url,
            'type_params' => serialize([
                'url' => $this->url,
            ]),
        ];
    }

}
